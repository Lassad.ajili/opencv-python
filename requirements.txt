# to install run the command : pip install -r requirements.txt
# Export current environment configuration file: pip freeze

# module == 0.6.1             # Version Matching. Must be version 0.6.1
# module >= 4.1.1            # Minimum version 4.1.1
# module != 3.5             # Version Exclusion. Anything except version 3.5
# module ~= 1.1        # Compatible release. Same as >= 1.1, == 1.*
# package >= 1.0, <=2.0


certifi==2020.4.5.1
numpy==1.18.4
opencv-python==4.2.0.34
wincertstore==0.2
pillow==7.1.2
opencv-contrib-python==4.2.0.34
